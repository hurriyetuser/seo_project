Click==7.0
configparser==3.7.3
Flask==1.0.2
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.1
selenium==3.141.0
urllib3==1.24.1
Werkzeug==0.14.1
pymssql==2.1.4
