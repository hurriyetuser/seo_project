#! /usr/bin/env python
# -*- coding: UTF-8 -*-
# @author: Ilkay Tevfik Devran
# @updatedDate: 18.03.2019
# @version: 2.2.0.0
#
# 
#

import re
import time
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import platform as plt
from lastFiveMinuteDataCreator import *
import multiprocessing
from localMSSQL.localDBMSSQL import local_Dbconnection as DB


# ----- WEB -------------------------------------------------------------------------------------------
def get_web_search_results(search_term, url='https:www.google.com.tr'):
    chrome_options = Options()
    prefs = {'profile.managed_default_content_settings.images':2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--lang=tr")
    browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    browser.get(url)

    # Search the search_tearm on google search box 
    search_box = browser.find_element_by_name('q')
    search_box.send_keys(search_term)
    search_box.submit()
    
    # get search on time
    readTime = datetime.now().strftime("%m/%d/%Y, %H:%M")
    readTime = datetime.strptime(readTime, "%m/%d/%Y, %H:%M")

    
    # Getting necessary carousel info from search page
    titles = [title.text for title in browser.find_elements_by_xpath("//div[@class='mRnBbe QgUve nDgy9d'][@role='heading']")]
    urls = [url.get_attribute('href') for url in browser.find_elements_by_xpath("//a[@style='text-decoration: none']")]
    published_time = [date.text for date in browser.find_elements_by_xpath("//div[@class='Z25Gce GJhQm']/span[@class='f']/span")]
    cites=[]
    for i, url in enumerate(urls):
        cite = re.sub(r'.*?(\.|/+)','', re.sub(r'(\.com|\.org|\.net|\.gov|\.tv)[^ ]*', '', url)).capitalize()    
        cites.append(cite)
    
    print (search_term, len(titles))#, len(urls), len(cites), len(published_time))
    """for i, e in enumerate(titles):
        print (str(i+1) + "\t" + cites[i] + "\t" + titles[i] + "\t" + urls[i] + "\t" + published_time[i])"""
    browser.quit()

    return titles, urls, cites, published_time, readTime


# ----- MOBILE ----------------------------------------------------------------------------------------
def get_mobile_search_results(search_term, url='https:www.google.com.tr'):
    chrome_options = Options()
    prefs = {'profile.managed_default_content_settings.images':2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--lang=tr")
    chrome_options.add_argument('--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1')
    browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    browser.get(url)

    # Search the search_tearm on google search box 
    search_box = browser.find_element_by_name('q')
    search_box.send_keys(search_term)
    search_box.submit()

    # get search on time
    readTime = datetime.now().strftime("%m/%d/%Y, %H:%M") 
    readTime = datetime.strptime(readTime, "%m/%d/%Y, %H:%M")

    # Check if there exists special carousels
    if len(browser.find_elements_by_xpath("//div[@class='fn6bCb']/span")) == 0:
        # Getting necessary carousel info from search page
        titles = [title.text for title in browser.find_elements_by_xpath("//div[@class='mRnBbe QgUve nDgy9d'][@style='height:3.75em;-webkit-line-clamp:3']")]
        urls = [url.get_attribute('href') for url in browser.find_elements_by_xpath("//g-inner-card[@class='xE0xid kno-fb-ctx QCSNec VoEfsd']/a[@href]")]
        published_time = [date.text for date in browser.find_elements_by_xpath("//div[@class='ovAMtc']/p[@class='S1FAPd']/span")]
        cites=[]
        for i, url in enumerate(urls):
            cite = re.sub(r'(.*?:/+|.*?\.)','', re.sub(r'(\.com|\.org|\.net|\.gov|\.tv)[^ ]*', '', re.sub(r'https://www.google.com.tr/amp/', '', urls[i]) ) )
            cites.append(cite.capitalize())
    
    else:
        # Take data of special carousels 
        titles = [title.text for title in (browser.find_elements_by_xpath("//div[@class='fn6bCb']/span"))]
        urls = [url.get_attribute('href') for url in browser.find_elements_by_xpath("//a[@class='eMoiSd amp_r']")]
        published_time = [date.text for date in browser.find_elements_by_xpath("//span[@class='uaCsqe']/span")]
        cites=[]
        for i, url in enumerate(urls):
            cite = re.sub(r'(.*?:/+|.*?\.)','', re.sub(r'.(com|org|gov).*', '', re.sub(r'https://www.google.com.tr/amp/', '', urls[i]) ) )
            cites.append(cite.capitalize())

        # Take data from classic carousels
        tmp_titles = [title.text for title in browser.find_elements_by_xpath("//div[@class='mRnBbe QgUve nDgy9d'][@style='height:3.75em;-webkit-line-clamp:3']")]
        tmp_urls = [url.get_attribute('href') for url in browser.find_elements_by_xpath("//g-inner-card[@class='xE0xid kno-fb-ctx QCSNec VoEfsd']/a[@href]")]
        tmp_published_time = [date.text for date in browser.find_elements_by_xpath("//div[@class='ovAMtc']/p[@class='S1FAPd']/span")]
        tmp_cites=[]
        for i, url in enumerate(tmp_urls):
            tmp_cite = re.sub(r'(.*?:/+|.*?\.)','', re.sub(r'.(com|org|net|gov).*', '', re.sub(r'https://www.google.com.tr/amp/', '', tmp_urls[i]) ) )
            tmp_cites.append(tmp_cite.capitalize())

        for i, val in enumerate(tmp_titles):
            titles.append(tmp_titles[i])
            urls.append(tmp_urls[i])
            published_time.append(tmp_published_time[i])
            cites.append(tmp_cites[i])
        

    print (search_term, len(titles))#, len(urls), len(cites), len(published_time))
    """for i, e in enumerate(titles):
        print (str(i+1) + "\t" + cites[i] + "\t" + titles[i] + "\t" + urls[i] + "\t" + published_time[i])"""
    browser.quit()
    return titles, urls, cites, published_time, readTime


# --- HELPER FUNCTIONS ---------------------------------------------------------------------------------
def start_search(search_term, db, platform=None):
    # DB connection
    db = DB()
    print("Start Search ", search_term[1])
    titles, urls, cites, published_time, readTime = None,None,None,None,None
    
    if platform == 'mobile':
        # Mobile Chrome Browser
        #print ("-- MOBILE --\n")
        titles, urls, cites, published_time, readTime = get_mobile_search_results(unicode(search_term[1]))
    elif platform == 'web':
        # Web Chrome Browser
        #print ("-- WEB --\n")
        titles, urls, cites, published_time, readTime = get_web_search_results(unicode(search_term[1]))
    else:
        print("[ERROR] Unknown platform " + platform)
        exit(1)
    
    if len(titles) != 0 and  len(urls) != 0 and len(cites) != 0 and len(published_time) != 0 :     
        for rank, (t, u, c, pt) in enumerate(zip(titles, urls, cites, published_time)):
            db.insert_search_results_to_db(platform, c, (rank+1), pt, t, u, search_term[0], readTime)
        print('[INFO]: Insertion has ben done\n')
    
    

# ----- MAIN -------------------------------------------------------------------------------------------
def main():
    print("\nCrawler Is Started...\n")

    platforms = ['web', 'mobile']

    # DB connection
    db = DB()

    # Get active search terms
    search_termss = db.get_active_words()
    
    # create batch list of words
    # In multiprocessing, each word in
    # a batch will run on a process
    batch_size = 7
    counter = 0
    words = []
    batch = []
    for w in search_termss:
        batch.append(w)
        counter+=1
        if counter % batch_size == 0 or counter==len(search_termss):
            words.append(batch)
            batch = []

    if len(search_termss) == 0:
        # Do NOT something if there is not any active search term
        print('[Warning]: There is no any term to be searched.\n\n \
            Crawler has been executed.\n\n')
    else:
        start_time = time.time()

        # Parallelized Part
        for platform in platforms:
            print ("--- "+ platform + " ---")
            for batch in words:
                tmp_p = []
                for i,word in enumerate(batch):
                    tmp_p.append(multiprocessing.Process(target=start_search, args=(word, db ,platform, )))
                    tmp_p[i].start()
                    #print("Process started")
                for p in tmp_p:        
                    p.join()

        duration = time.time() - start_time
        print(duration)

    

if __name__ == "__main__":
    # start crawling
    main()

    # after crawling is finished
    # prepare json file for last 
    # 5 mins data to be served
    prepare_5_mins_data()



